
# Customize to your needs...

#aliases
alias ev="vim ~/.vimrc"
alias ez="vim ~/.zshrc"
alias ezr="vim ~/.zpreztorc"
alias et="vim ~/.tmux.conf"
#start the applciatin on run



#liquid prompt 
#need liquid prompt to respect the themes of prezto , then I will activate it.
#[[ $- = *i* ]] && source ~/liquidprompt/liquidprompt

#aliases
alias update='sudo apt-get update'
alias upgrade='sudo apt-get upgrade'
alias updgr='sudo apt-get update && sudo apt-get upgrade'

##tmux
tmux -2u


#aliases shortcuts
alias internet='ping -c6 8.8.8.8'
