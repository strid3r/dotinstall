#!/bin/bash
set -e
#default frameworkfile
#if by any chance and coincidence you are viewing this , it's just N00B file , dont use it
#Last revision date :- Sun Feb 26 21:56:41 IST 2017
function install_package {
	sudo apt update
	sudo apt-get install codeblocks-common -y 
	sudo apt-get install vim tmux libncurses5-dev gcc make git exuberant-ctags zsh curl build-essential libssl-dev -y 
}

function vim_install {
	echo "installing vundle setup script"
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	echo "installing all plugins from vundle vim script"
	sleep 5
	ln -sf $(pwd)/vimrc     $HOME/.vimrc
	vim +PluginInstall +qall
}

zhrc="$HOME/.zshrc"
#next line will check whether Xorg server is running or not and install all the package based upon that
if pgrep -x "Xorg" > /dev/null 
then
    echo "Xorg is running , installing packages"
#	install_package
else
	echo "Xorg is not running"
	sudo apt-get install vim tmux libncurses5-dev gcc make git exuberant-ctags libssl-dev zsh curl build-essential
fi
#zim installation a ZIM - Zsh IMproved framework
#checking whether zsh is presnt or not
if [ $(dpkg-query -W -f='${Status}' zsh 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
	echo "zsh isn't present,installing it"
	sudo apt-get install zsh git -y
else
	echo "zsh is present"
fi
sleep 10
echo "changing default shell"
sudo chsh --shell=/bin/zsh $USER
if [ -f "$zhrc" ]
then
	echo "previous installation find , taking a backup and purging it"
	tar -cvzf zpre-$(date +%Y-%m-%d).tar.gz $HOME/.z*
	rm -rf $HOME/.z*
else
	echo "no previous installtion find"
fi
echo "cloning zim in home"
git clone --recursive https://github.com/Eriner/zim.git ${ZDOTDIR:-${HOME}}/.zim
sleep 10
chmod a+x ziminstall.sh
zsh ziminstall.sh
echo "Open a new terminal and finish optimization (this is only needed once, hereafter it will happen upon desktop/tty login)
source ${ZDOTDIR:-${HOME}}/.zlogin"
###############################################################################
ln -sf $(pwd)/tmux.conf $HOME/.tmux.conf
echo "installing vundle setup script"
vim_install
#######################################################################
sleep 5
#echo "which shell do you want for  user "$USER."
#echo "enabling bash-it plugins and aliases"
#bash-it enable plugin docker docker-compose git vim python aws
#bash-it enable alias vim git docker docker-compose ag
#bash-it enable completion virtualbox ssh pip git git_flow django docker docker-compose makefile

#############################################################################################
echo "appending custom zshrc"
cat $(pwd)/zshrc >> $HOME/.zshrc
#############################################################################
echo "Changing default theme and editor in .zimrc"
read -p "Are you sure? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
        sed -i 's/steeef/eriner/' $HOME/.zimrc
#        sed -i 's/emacs/vi/' $HOME/.zimrc
fi

function fonts_configure {
	wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf
	wget https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf
	sudo mv PowerlineSymbols.otf /usr/share/fonts/
	sudo fc-cache -vf
	sudo mv 10-powerline-symbols.conf /etc/fonts/conf.d/
}
echo "installing powerline fonts"
fonts_configure
echo "goodbye"