# DotFiles

 Custom deployment script to install all necessary package in a fresh installtion of ubuntu flavorus

 - To install run  `./deploy.sh `
 - To install powerline fonts ( Necessary for many themes)
 - https://github.com/zimfw/zimfw/issues/169

>  - wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf
>  - wget https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf
>  - sudo mv PowerlineSymbols.otf /usr/share/fonts/
>  - sudo fc-cache -vf
>  - sudo mv 10-powerline-symbols.conf /etc/fonts/conf.d/

>  - $ cd
>  - $ git clone https://github.com/gpakosz/.tmux.git
>  - $ ln -s -f .tmux/.tmux.conf
>  - $ cp .tmux/.tmux.conf.local 

-   https://github.com/agnoster/agnoster-zsh-theme

-  To test execute this on terminal to check fonts compatiblity .
> `echo "\ue0b0 \u00b1 \ue0a0 \u27a6 \u2718 \u26a1 \u2699"`


> Custom colors for gnome-terminal
https://mayccoll.github.io/Gogh/


> install additional color scheme for terminal 

- https://github.com/Mayccoll/Gogh

> to enable powerline font in tmux with above config add this filw with followig config

  -  sudo vim /etc/locale.conf
  -  LC_ALL=en_US.UTF-8
  -  

> install sourcecode pro

  - wget https://github.com/adobe-fonts/source-code-pro/archive/2.030R-ro/1.050R-it.zip
  - cp source-code-pro-*-it/OTF/*.otf ~/.fonts/
  - fc-cache -f -v


  
