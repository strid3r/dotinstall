"vundle
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
"git interface
Plugin 'tpope/vim-fugitive'
"filesystem
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'kien/ctrlp.vim'
"This plugin is meant to help you respecting the Linux kernel coding style,
"described at:http://www.kernel.org/doc/Documentation/CodingStyle
Plugin 'linuxsty.vim'
"nice theme
Plugin 'vim-airline/vim-airline'
Plugin 'klen/rope-vim'
Plugin 'ervandew/supertab'
""code folding
Plugin 'tmhedberg/SimpylFold'
"Source code browser (supports C/C++, java, perl, python, tcl, sql, php, etc)
Plugin 'taglist.vim'
"Colors!!!
Plugin 'altercation/vim-colors-solarized'
Plugin 'jnurmine/Zenburn'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'morhetz/gruvbox'
call vundle#end()

filetype plugin indent on    " enables filetype detection
let g:SimpylFold_docstring_preview = 1

"autocomplete
let g:ycm_autoclose_preview_window_after_completion=1
"yaml identation making tab to work like space
autocmd FileType yaml setlocal ai ts=2 sw=2 et
"Load the airline theme
let g:airline_theme='luna'
let g:airline_powerline_fonts = 1

"custom keys
let mapleader=" "
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
"
call togglebg#map("<F5>")
"colorscheme hybrid "make sure it have installed in ~/.vim/colors 
"colorscheme mrkn256 "donwload link https://github.com/mrkn/mrkn256.vim
set background=dark
colorscheme gruvbox

set noswapfile "I don't like swap files 
syn on se title
set tabstop=8
set softtabstop=8
set shiftwidth=8
set noexpandtab
set number
set encoding=utf-8
"set relativenumber
set splitbelow  "where the split is going to happen
set splitright
set backspace=indent,eol,start
"split navigations
nnoremap <C-J> <C-W><C-J>	"Ctrl-j move to the split below
nnoremap <C-K> <C-W><C-K>	"Ctrl-k move to the split above
nnoremap <C-L> <C-W><C-L>	"Ctrl-l move to the split to the right
nnoremap <C-H> <C-W><C-H>	"Ctrl-h move to the split to the left
" Enable folding
set foldmethod=indent
set foldlevel=99
set t_Co=256
" 80 characters line
set colorcolumn=81
"execute "set colorcolumn=" . join(range(81,335), ',')
highlight ColorColumn ctermbg=Black ctermfg=DarkRed
" Highlight trailing spaces
" " http://vim.wikia.com/wiki/Highlight_unwanted_spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

"___---------------------------------END USER SETTINGS------------------------_____

